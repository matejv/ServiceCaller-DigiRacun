﻿using System;
using System.Net;
using System.Threading;

namespace ServiceCaller_DigiRacun
{
    class Program
    {
        private static readonly string serviceUrl = "https://digiracun.azurewebsites.net/Service2.svc/{0}";
        static ManualResetEvent resetEventReceipt = new ManualResetEvent(false);
        static ManualResetEvent resetEventProduct = new ManualResetEvent(false);

        // calls web service method for sending guarantee expiration notifications on receipts
        private static async void guaranteeExpirationReceipt(string key)
        {
            HttpWebRequest request = HttpWebRequest.CreateHttp(String.Format(serviceUrl, "guaranteeExpirationEmailReceipt"));
            request.Headers.Add(HttpRequestHeader.Authorization, key);
            request.Method = "POST";
            request.ContentLength = 0;


            try
            {
                HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine(String.Format("Guarantee expiration notifications on receipts succesfully sent (status: {0})\n", response.StatusCode));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("ERROR occurred when sending request for guarantee expiration on receipts to web service:\n{0}\n", e.Message));
            }
            finally
            {
                request.Abort();
                resetEventReceipt.Set();
            }
        }

        // calls web service method for sending guarantee expiration notifications on products
        private static async void guaranteeExpirationProduct(string key)
        {
            HttpWebRequest request = HttpWebRequest.CreateHttp(String.Format(serviceUrl, "guaranteeExpirationEmailProduct"));
            request.Headers.Add(HttpRequestHeader.Authorization, key);
            request.Method = "POST";
            request.ContentLength = 0;

            try
            {
                HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine(String.Format("Guarantee expiration notifications on products succesfully sent (status: {0})\n", response.StatusCode));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("ERROR occurred when sending request for guarantee expiration on products to web service:\n{0}\n", e.Message));
            }
            finally
            {
                request.Abort();
                resetEventProduct.Set();
            }
        }

        static void Main(string[] args)
        {
            guaranteeExpirationReceipt("EAD771D65EC0450B99E10F1E286E8F2E");
            resetEventReceipt.WaitOne();

            guaranteeExpirationProduct("3897CE504744442EBD0F818059E4CA3B");
            resetEventProduct.WaitOne();
        }
    }
}
